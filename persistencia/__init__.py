from pony.orm import *

db = Database()


class Cliente(db.Entity):
    nombre = Required(str)
    telefono = Required(str)
    autos = Set('Auto')
    

class Auto(db.Entity):
    marca = Required(str)
    modelo = Required(str)
    chapa = Required(str)
    tipo = Required(str)
    fecha = Required(str)
    hora = Required(str)
    observaciones = Set('Observacion')
    cliente = Required(Cliente)
    

class Categoria(db.Entity):
    nombre = Required(str)
    subsistemas = Set('Subsistema')
    observaciones = Set('Observacion')


class Subsistema(db.Entity):
    nombre = Required(str)
    categoria = Required(Categoria)
    observaciones = Set('Observacion')
    

class Observacion(db.Entity):
    comentario = Required(str)
    estado = Required(int)
    auto = Required(Auto)
    categoria = Required(Categoria)
    subsistema = Required(Subsistema)


