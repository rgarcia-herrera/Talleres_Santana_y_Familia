"""
Carga categorias y subsistemas a la base de datos
"""

from pony.orm import *
from persistencia import *

set_sql_debug(True)

db.bind(provider='sqlite',
        filename='database.sqlite',
        create_db=True)
db.generate_mapping(create_tables=True)


# Categorías y sus subsistemas
cats = {
    'Sistema eléctrico': [
        'dinamo',
        'motor ',
        'regulador',
        'baterias',
        'cables ',
    ],

    'Paneles de Luces Delanteras': [
        'De Carretera',
        'De Poblacion',
    ],
    
    'Paneles de Luces Traseras': [
        'De Poblacion',
        'De Freno',
        'De Contramarcha',
    ],

    'Luces Direccionales': [
        'Delanteras',
        'Laterales',
        'Traseras',
    ],

    'Luces Interiores': [
        'De Techo',
        'De Puertas',
        'Luz de Chapa',
        'Faro Direccional',
        'Bobina de Ignicion',
        'Bujias',
        'Delco',
        'Relay de Luces',
        'Relay de Pito',
        'Caja de Fusibles',
        'Laminaria de Pizarra',
    ],
    
    'Complementos del motor': [
        'Purificador',
        'Caraburador/Bomba de Inyeccion',
        'Filtro de aceite',
        'Filtro de Combustible',
        'Correas',
        'Varilla de Nivel de aceite',
        'Radiador',
        'Tapa de Radiador',
        'Mangueras',
        'Bomba de Gasolina',
        'Aire acondicionado',
    ],

    'Carroceria y Neumaticos': [
        'Guardafangos delanteros',
        'Guardafangos traseros',
        'Neumaticos delanteros',
        'Neumaticos traseros',
        'Neumaticos de repuesto',
        'Tapas de ruedas',
        'Clanes con Tuercas',
        'Caja de velocidades',
        'Diferencial',
        'Transmision',
    ],

    'Pizarra': [
        'Cuenta Millas',
        'Odometro',
        'Control de combustible',
        'Control de aceite',
        'Control de Carga',
        'Control de Combustible',
        'Comando',
        'Casetera',
        'Cenisero',
        'Encendedor',
        'Rejillas distribuidoras de aire',
        'Tapa de guantera',
        'Bocinas',
        'Interruptor de arranque',
        'Panel de Control del Aire',
        'Planta de radio',
        'Pilotos indicadores',
    ],

    'Cabina': [
        'Bola de Palanca de Cambios',
        'Agarraderas Interiores',
        'Cinturones de Seguridad',
        'Puertas Delanteras',
        'Puertas traseras',
        'Puertas Laterales',
        'Puerta de fondo',
        'Cristales de Puertas Delanteras',
        'Cristales de Puertas Traseras',
        'Cristales de Puertas Laterales',
        'Cristales de puertas de fondo',
        'Seguros de Puertas',
        'Manivelas de abrir  Puertas',
        'Manivelas de subir Cristales',
        'Panos de Contrapuertas',
        'Asientos Delanteros',
        'Asientos Traseros',
        'Forros de Asientos',
        'Tapa Sol Izquierdo y derecho',
        'Parabrisas Delantero',
        'Retrovisor Interior',
        'Alfombras',
    ]}



with db_session:
    for c in cats:
        cat = Categoria(nombre=c)
        for s in cats[c]:
            sys = Subsistema(nombre=s,
                             categoria=cat)

