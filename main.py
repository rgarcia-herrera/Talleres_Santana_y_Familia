from pony.orm import *
from persistencia import *

from kivy.app import App
from kivy.uix.button import Button
from kivy.lang import Builder
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import *
from kivy.uix.textinput import TextInput
Builder.load_file("./main.kv")

set_sql_debug(True)

db.bind(provider='sqlite',
        filename='database.sqlite',
        create_db=False)
db.generate_mapping(create_tables=False)


class PantallaPrincipal(Screen):
    pass

class SegundaPantalla(Screen):
    pass
     
class TerceraPantalla(Screen):
    pass

class MyApp(App):

    @db_session
    def agrega_cliente(self, nombre, telefono):
        """ 
        Ejemplo de como usar pony para
        crear registros en la base de datos
        usando el modelo en persistencia/__init__.py
        """
        return Cliente(nombre=nombre,
                       telefono=telefono)
        
    @db_session        
    def agrega_observacion(self):
        # este método necesita:
        # comentario, estado,
        # auto (auto necesita cliente)
        # categoria, subsistema.
        # Debería recibirlos como argumentos o tal vez sean atributos
        # de la clase MyApp.
        # En este ejemplo voy a crear auto y cliente, voy a buscar
        # categoria y subcategoria para vincularlos a la observacion.

        # crear cliente con el método de ahi arriba
        rodrigo = self.agrega_cliente(nombre='Rodrigo Garcia',
                                      telefono='5533445234')

        # agregar automovil, vinculado al cliente
        isuzu_amigo = Auto(marca='Isuzu', modelo='Amigo 1999',
                           chapa='gris plata', tipo='SUV',
                           fecha='2023-09-23', hora='23:45',
                           cliente=rodrigo)

        # buscamos categoria y subcategoria para poder vincularlas
        cabina = Categoria.get(nombre='Cabina')
        alfombras = Subsistema(nombre='Alfombras')

        # creamos una observacion
        observacion = Observacion(categoria=cabina,
                                  subsistema=alfombras,
                                  auto=isuzu_amigo,
                                  estado=-3,
                                  cometario="Alfombras sucias y rotas, favor de reparar y limpiar")
        
    
    def build(self):
        sm=ScreenManager(transition=WipeTransition())
        sm.add_widget(PantallaPrincipal(name='inicio'))
        sm.add_widget(SegundaPantalla(name='segundapantalla'))
        sm.add_widget(TerceraPantalla(name='tercerapantalla'))
        return sm


if __name__ == '__main__':
    MyApp().run()
